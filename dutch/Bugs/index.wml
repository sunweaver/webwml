#use wml::debian::template title="Debians bugvolgsysteem" BARETITLE=true NOCOPYRIGHT=true
#include "$(ENGLISHDIR)/Bugs/pkgreport-opts.inc"
#{#style#:<link rel="stylesheet" href="https://bugs.debian.org/css/bugs.css" type="text/css">:##}
{#meta#:
<script type="text/javascript" src="hashbug_redirect.js"></script>
:#meta#}

#use wml::debian::translation-check translation="07162807462d48f6282cbf61386e273d9a8544fd"
# $Author$
# $Date$

<p>Debian heeft een bugvolgsysteem (bug tracking system - BTS) waarmee gebruikers en
ontwikkelaars bugs kunnen melden en bijhouden.  Elke bug krijgt een
uniek nummer en de informatie wordt net zolang bewaard totdat de bug is
opgelost.</p>


<h2>Hoe u in Debian een bug kunt melden</h2>

<p>Op een afzonderlijke pagina zijn instructies en tips opgenomen over
<a href="Reporting">hoe u een bug kunt melden</a> als u
tegen een probleem in de Debian distributie aanloopt.</p>

<h2>Documentatie over het bugvolgsysteem</h2>

<ul>
  <li><a href="Developer">Geavanceerde informatie over het
        gebruik van het bugvolgsysteem</a></li>
  <li><a href="server-control">Informatie over het bewerken van
        bugmeldingen via een e-mailinterface</a></li>
  <li><a href="server-refcard">Referentiekaart voor de e-mailinterface</a></li>
  <li><a href="Access">Methoden om bugmeldingen te bekijken</a></li>
  <li><a href="server-request">Bugmeldingen opvragen via e-mail</a></li>
</ul>

<h2>Bekijken van bugmeldingen via het WWW</h2>

<p style="text-align:center">
<img src="https://qa.debian.org/data/bts/graphs/all.png?m=0.8&amp;h=250&amp;w=600"
alt="Totaal aantal bugs">
</p>

<p>Zoek een bug op <strong>nummer</strong>:
  <br />
  <a name="bugreport"></a>
  <form method="get" action="https://bugs.debian.org/cgi-bin/bugreport.cgi">
  <p>
  <input type="text" size="9" name="bug" value="">
  <label><input type="checkbox" name="mbox" value="yes"> als mbox</label>
  <label><input type="checkbox" name="trim" value="no"> toon alle kopteksten</label>
  <label><input type="checkbox" name="boring" value="yes"> toon oninteressante berichten</label>
  <input type="submit" value="Zoek">
  </p>
  </form>

<h2>Selecteren van bugmeldingen via het WWW</h2>

<bts_main_form>

<table class="forms">

<tr><td><h2>Bugs selecteren</h2>
</td>
<td>
<bts_select_form>
</td>
<td>
<p>Na de eerste zoekopdracht kunnen aanvullende selecties worden toegevoegd.
   Als een latere selectie op hetzelfde zoekveld wordt gedaan, dan wordt de
   verzameling van de resultaten getoond; als deze op een ander zoekveld gebeurt, dan
   wordt de intersectie van de resultaten getoond.</p>
<p>Geldige waarden voor ernst zijn <bts_severities_all>.</p>
<p>Geldige labels zijn <bts_tags>.</p>
</td>
</tr>

<tr><td><h2>Bugs insluiten</h2></td>
<td>
<bts_include_form>
</td>
<td>
</td>
</tr>

<tr><td><h2>Bugs uitsluiten</h2></td>
<td>
<bts_exclude_form>
</td>
<td></td>
</tr>

<tr><td><h2>Indelen op basis van</h2></td>
<td></td>
</tr>
<tr><td><h2>Sorteren op</h2></td>
<td>
<bts_orderby_form>
</td>
<td></td>
</tr>

<tr><td><h2>Diverse opties</h2></td>
<td>
<bts_miscopts_form>
</td>
</tr>

<tr><td><h2>Verzenden</h2></td><td colspan=2>
<input type="submit" name="submit" value="Verzend">
</td></tr>

</table>
</form>

<p>De bovenstaande zoekopdrachten kunnen ook uitgevoerd worden door
URL’s van de volgende vorm direct aan te roepen:</p>
<ul>
  <li><tt>https://bugs.debian.org/<var>nummer</var></tt></li>
  <li><tt>https://bugs.debian.org/mbox:<var>nummer</var></tt></li>
  <li><tt>https://bugs.debian.org/<var>pakket</var></tt></li>
  <li><tt>https://bugs.debian.org/src:<var>broncodepakket</var></tt></li>
  <li><tt>https://bugs.debian.org/<var>ontwikkelaar@e-mail.adres</var></tt></li>
  <li><tt>https://bugs.debian.org/from:<var>melder@e-mail.adres</var></tt></li>
  <li><tt>https://bugs.debian.org/severity:<var>ernst</var></tt></li>
  <li><tt>https://bugs.debian.org/tag:<var>label</var></tt></li>
</ul>

<h2>Zoeken naar bugmeldingen</h2>

## Link naar bugs-search.d.o verwijderd wegens Bug#629645 (dienst gesloten):
#<p>U kunt naar bugrapporten zoeken met onze op HyperEstraier gebaseerde
#<a href="https://bugs.debian.org/cgi-bin/search.cgi">zoekmachine</a>.</p>

<p>De 'Ultimate Debian Database' (UDD) heeft een <a href="https://udd.debian.org/bugs/">zoekmachine voor bugs</a> waarmee op basis
van verschillende criteria gezocht kan worden.</p>

<p>Een alternatieve manier om naar bugmeldingen te zoeken is
<a href="https://groups.google.com/d/forum/linux.debian.bugs.dist">Google Groups</a>
De te doorzoeken periode kan worden beperkt via de optie
<a href="https://groups.google.com/d/search/group%3Alinux.debian.bugs.dist">\
geavanceerd zoeken</a>.</p>

<p>Een andere site die je toelaat om naar bugmeldingen te zoeken is
<a href="http://www.mail-archive.com/debian-bugs-dist%40lists.debian.org/">The
Mail Archive</a>.</p>

<h2>Extra informatie</h2>

<p>De huidige lijst van <a href="https://bugs.debian.org/release-critical/">\
release-kritieke bugs</a>.</p>

<p>De lijst met <a href="pseudo-packages">pseudo-pakketten</a>
die het Debian bugvolgsysteem momenteel kent.</p>

<p>De volgende indices van bugmeldingen zijn beschikbaar:</p>

<ul>
  <li>Pakketten met
      <a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=pkg">actieve</a>
      en
      <a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=pkg&amp;archive=yes">gearchiveerde</a>
      bugmeldingen.</li>
  <li>Broncode-pakketten met
      <a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=src">actieve</a>
      en
      <a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=src&amp;archive=yes">gearchiveerde</a>
      bugmeldingen.</li>
  <li>Ontwikkelaars van pakketten met
      <a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=maint">actieve</a>
      en
      <a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=maint&amp;archive=yes">gearchiveerde</a>
      bugmeldingen.</li>
  <li>Rapporteurs van
      <a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=submitter">actieve</a>
      en
      <a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=submitter&amp;archive=yes">gearchiveerde</a>
      bugmeldingen.</li>
</ul>

<p><strong>N.B.</strong> Sommige van de indices die voorheen via deze
webpagina konden worden opgevraagd, zijn op het moment niet beschikbaar.
Een en ander is het gevolg van interne problemen bij het programma dat
gebruikt werd om deze indices te genereren. Onze excuses voor het
eventuele ongemak.</p>

<h2>Spam melden</h2>

<p>Het bugvolgsysteem krijgt regelmatig spam. U kunt spam in het
bugvolgsysteem melden door de bug <a href="#bugreport">op nummer</a> op te
zoeken en op de link "this bug log contains spam" (de opvolging van deze bug bevat spam) onderaan de pagina te
klikken.</p>

#include "$(ENGLISHDIR)/Bugs/footer.inc"
