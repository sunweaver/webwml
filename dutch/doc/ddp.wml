#use wml::debian::ddp title="Debian Documentatie Project"
#use wml::debian::translation-check translation="f99f15c4ec523898c4316572025b17732b7a9e87"

# Translator: $Author$
# Last update: $Date$

<p>Het Debian Documentatie Project heeft als doel alle inspanningen om meer
en betere documentatie voor Debian te schrijven, te coördineren.</p>

  <h2>DDP-werk</h2>
<div class="line">
  <div class="item col50">

    <h3>Handleidingen</h3>
    <ul>
	  <li><strong><a href="user-manuals">Handleidingen voor
	      gebruikers</a></strong></li>
	  <li><strong><a href="devel-manuals">Handleidingen voor
	      ontwikkelaars</a></strong></li>
      <li><strong><a href="misc-manuals">Diverse handleidingen</a></strong></li>
      <li><strong><a href="#other">Problematische
	      handleidingen</a></strong></li>
    </ul>

    <h3>Huidige punten van aandacht</h3>
    <ul>
      <li><a href="topics">Onderwerpen</a> voor discussie en om op te
	      lossen.</li>
    </ul>

    <h3>Nog te doen</h3>
    <ul>
      <li><a href="todo">Dingen die gedaan moeten worden</a>;
	      vrijwilligers zijn altijd welkom!</li>
      <li><a href="todo#ideas">Goede en slechte ideeën</a>, grotere
    	  problemen die opgelost moeten worden.</li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h3><a href="docpolicy">Beleid m.b.t. documentatie</a></h3>
    <ul>
      <li>De licenties van de handleidingen moeten voldoen aan de DFSG.</li>
      <li>Mapstructuur: bestandssysteem, WWW, FTP.</li>
	  <li>We gebruiken Docbook XML voor onze documenten. Het gebruik van
	      DebianDoc SGML wordt uitgefaseerd.</li>
      <li>Elk document heeft één beheerder.</li>
    </ul>

    <h3>Links</h3>
    <ul>
      <li><a href="https://lists.debian.org/debian-doc/">\
	      E-maillijst-archief van debian-doc</a></li>
#      <li><a href="https://www.debian.org/~elphick/ddp/linuxdoc-sgml.ps">
#          Linuxdoc-SGML</a> article in the Linux Journal</li>
      <li><a href="http://tldp.org/LDP/LGNET/issue15/debian.html">Debian
		  Linux Installation &amp; Getting Started</a> (“Beginnen met en
		  installeren van Debian”) artikel in de Linux Gazette.</li>
    </ul>

    <h3>Git-toegang</h3>
    <ul>
	  <li><a href="vcs">Hoe toegang te krijgen tot</a> de git-opslagplaats van
	      het DDP.</li>
      <li>DDP git-documenten in de <a href="manuals/">onderliggende map
          handleidingen</a>.</li>
    </ul>

  </div>

</div>

<hr class="clr">
<p>
Portugeessprekenden opgelet: Bezoek <a
href="http://wiki.debianbrasil.org/">DDP-BR</a>, webpagina’s over lokalisatie
van de Debian documentatie naar het Braziliaans Portugees.
</p>
<hr />

<h2><a name="other">Problematische handleidingen</a></h2>

<p>In aanvulling op de handleidingen die op de gebruikelijke manier bekend
gemaakt worden, onderhouden we ook de volgende handleidingen. Deze zijn
op de een of andere manier problematisch, dus we raden deze niet aan aan
alle gebruikers. Gebruik op eigen risico.</p>

<ul>
  <li><a href="obsolete#tutorial">Debian Tutorial</a>, vervangen door
      de <a href="user-manuals#guide">Debian Gids</a></li>
  <li><a href="obsolete#userref">Debian User Reference Manual</a>,
      wordt niet meer bijgewerkt, behoorlijk onvolledig</li>
  <li><a href="obsolete#system">Debian System Administrator’s
      Manual</a>, wordt niet meer bijgewerkt, bijna leeg</li>
  <li><a href="obsolete#network">Debian Network Administrator’s
      Manual</a>, wordt niet meer bijgewerkt, onvolledig</li>
  <li><a href="devel-manuals#swprod">How Software Producers can distribute
      their products directly in .deb format</a>, wordt niet meer bijgewerkt
      verouderd</li>
  <li><a href="devel-manuals#packman">Debian Packaging Manual</a>, gedeeltelijk
      opgenomen in de <a href="devel-manuals#policy">Debian Policy Manual</a>,
      de rest zal worden opgenomen in een dpkg referentiehandleiding
      die nog moet worden geschreven.</li>
  <li><a href="obsolete#makeadeb">Introduction: Making a Debian
      Package</a> achterhaald door de
      <a href="devel-manuals#maint-guide">Debian New Maintainers’ Guide</a></li>
  <li><a href="obsolete#programmers">Debian Programmers’ Manual</a>,
      achterhaald door de
      <a href="devel-manuals#maint-guide">Debian New Maintainers’ Guide</a>
      en
      <a href="devel-manuals#debmake-doc">Guide for Debian Maintainers</a></li>
  <li><a href="obsolete#repo">Debian Repository HOWTO</a>, verouderd na de
      introductie van veilig APT</li>
</ul>
