# translation of templates.po to Ukrainian
# Eugeniy Meshcheryakov <eugen@debian.org>, 2004, 2005, 2006.
# Volodymyr Bodenchuk <Bodenchuk@bigmir.net>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: templates\n"
"PO-Revision-Date: 2017-11-17 01:30+0200\n"
"Last-Translator: Volodymyr Bodenchuk <Bodenchuk@bigmir.net>\n"
"Language-Team: українська <Ukrainian <ukrainian>>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Gtranslator 2.91.7\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n "
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../english/template/debian/wnpp.wml:8
msgid "No requests for adoption"
msgstr "Запитів про „усиновлення“ не поступало"

#: ../../english/template/debian/wnpp.wml:12
msgid "No orphaned packages"
msgstr "Немає „осиротілих“ пакунків"

#: ../../english/template/debian/wnpp.wml:16
msgid "No packages waiting to be adopted"
msgstr "Немає пакунків, що очікують на „усиновлення“"

#: ../../english/template/debian/wnpp.wml:20
msgid "No packages waiting to be packaged"
msgstr "Немає пакунків, по яким ведеться робота"

#: ../../english/template/debian/wnpp.wml:24
msgid "No Requested packages"
msgstr "Немає запитів на створення пакунків"

#: ../../english/template/debian/wnpp.wml:28
msgid "No help requested"
msgstr "Немає запитів про допомогу"

#. define messages for timespans
#. first the ones for being_adopted (ITAs)
#: ../../english/template/debian/wnpp.wml:34
msgid "in adoption since today."
msgstr "„усиновлення“ почалося сьогодні."

#: ../../english/template/debian/wnpp.wml:38
msgid "in adoption since yesterday."
msgstr "„усиновлення“ почалося вчора."

#: ../../english/template/debian/wnpp.wml:42
msgid "%s days in adoption."
msgstr "„усиновлення“ почалося %s днів тому."

#: ../../english/template/debian/wnpp.wml:46
msgid "%s days in adoption, last activity today."
msgstr "„усиновлення“ почалося %s днів тому, остання активність була сьогодні."

#: ../../english/template/debian/wnpp.wml:50
msgid "%s days in adoption, last activity yesterday."
msgstr "„усиновлення“ почалося %s днів тому, остання активність була вчора."

#: ../../english/template/debian/wnpp.wml:54
msgid "%s days in adoption, last activity %s days ago."
msgstr ""
"„усиновлення“ почалося %s днів тому, остання активність була %s днів тому."

#. timespans for being_packaged (ITPs)
#: ../../english/template/debian/wnpp.wml:60
msgid "in preparation since today."
msgstr "підготовка почалась сьогодні."

#: ../../english/template/debian/wnpp.wml:64
msgid "in preparation since yesterday."
msgstr "підготовка почалась вчора."

#: ../../english/template/debian/wnpp.wml:68
msgid "%s days in preparation."
msgstr "підготовка почалась %s днів тому."

#: ../../english/template/debian/wnpp.wml:72
msgid "%s days in preparation, last activity today."
msgstr "підготовка почалась %s днів тому, остання активність була сьогодні."

#: ../../english/template/debian/wnpp.wml:76
msgid "%s days in preparation, last activity yesterday."
msgstr "підготовка почалась %s днів тому, остання активність була вчора."

#: ../../english/template/debian/wnpp.wml:80
msgid "%s days in preparation, last activity %s days ago."
msgstr ""
"підготовка почалась %s днів тому, остання активність була %s днів тому."

#. timespans for request for adoption (RFAs)
#: ../../english/template/debian/wnpp.wml:85
msgid "adoption requested since today."
msgstr "запит на „усиновлення“ створений сьогодні."

#: ../../english/template/debian/wnpp.wml:89
msgid "adoption requested since yesterday."
msgstr "запит на „усиновлення“ створений вчора."

#: ../../english/template/debian/wnpp.wml:93
msgid "adoption requested since %s days."
msgstr "запит на „усиновлення“ створений %s днів тому."

#. timespans for orphaned packages (Os)
#: ../../english/template/debian/wnpp.wml:98
msgid "orphaned since today."
msgstr "„осиротів“ сьогодні."

#: ../../english/template/debian/wnpp.wml:102
msgid "orphaned since yesterday."
msgstr "„осиротів“ вчора."

#: ../../english/template/debian/wnpp.wml:106
msgid "orphaned since %s days."
msgstr "„осиротів“ %s днів тому."

#. time spans for requested (RFPs) and help requested (RFHs)
#: ../../english/template/debian/wnpp.wml:111
msgid "requested today."
msgstr "запит створений сьогодні."

#: ../../english/template/debian/wnpp.wml:115
msgid "requested yesterday."
msgstr "запит створений вчора."

#: ../../english/template/debian/wnpp.wml:119
msgid "requested %s days ago."
msgstr "запит створений %s днів тому."

#: ../../english/template/debian/wnpp.wml:133
msgid "package info"
msgstr "інформація про пакунок"

#. popcon rank for RFH bugs
#: ../../english/template/debian/wnpp.wml:139
msgid "rank:"
msgstr "ранг:"
