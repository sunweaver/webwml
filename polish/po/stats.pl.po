msgid ""
msgstr ""
"PO-Revision-Date: 2012-08-17 18:10+0200\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Statystyki tłumaczeń strony WWW Debiana"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Jest %d stron do przetłumaczenia."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Jest %d bajtów do przetłumaczenia."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Jest %d napisów do przetłumaczenia."

#: ../../stattrans.pl:278 ../../stattrans.pl:494
msgid "Wrong translation version"
msgstr "Nieprawidłowa wersja tłumaczenia"

#: ../../stattrans.pl:280
msgid "This translation is too out of date"
msgstr "To tłumaczenie jest przestarzałe."

#: ../../stattrans.pl:282
msgid "The original is newer than this translation"
msgstr "Oryginał jest nowszy niż to tłumaczenie"

#: ../../stattrans.pl:286 ../../stattrans.pl:494
msgid "The original no longer exists"
msgstr "Oryginał już nie istnieje"

#: ../../stattrans.pl:470
msgid "hit count N/A"
msgstr "liczba odwiedzin nie dostępna"

#: ../../stattrans.pl:470
msgid "hits"
msgstr "odwiedzin(y)"

#: ../../stattrans.pl:488 ../../stattrans.pl:489
msgid "Click to fetch diffstat data"
msgstr "Kliknij by pobrać statystyki zmian"

#: ../../stattrans.pl:599 ../../stattrans.pl:739
msgid "Created with <transstatslink>"
msgstr ""

#: ../../stattrans.pl:604
msgid "Translation summary for"
msgstr "Podsumowanie tłumaczeń dla"

#: ../../stattrans.pl:607 ../../stattrans.pl:763 ../../stattrans.pl:809
#: ../../stattrans.pl:852
msgid "Not translated"
msgstr "Nie przetłumaczone"

#: ../../stattrans.pl:607 ../../stattrans.pl:762 ../../stattrans.pl:808
msgid "Outdated"
msgstr "Zdezaktualizowane"

#: ../../stattrans.pl:607
msgid "Translated"
msgstr "Przetłumaczone"

#: ../../stattrans.pl:607 ../../stattrans.pl:687 ../../stattrans.pl:761
#: ../../stattrans.pl:807 ../../stattrans.pl:850
msgid "Up to date"
msgstr "Aktualne"

#: ../../stattrans.pl:608 ../../stattrans.pl:609 ../../stattrans.pl:610
#: ../../stattrans.pl:611
msgid "files"
msgstr "plików"

#: ../../stattrans.pl:614 ../../stattrans.pl:615 ../../stattrans.pl:616
#: ../../stattrans.pl:617
msgid "bytes"
msgstr "bajtów"

#: ../../stattrans.pl:624
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Uwaga: lista stron jest posortowana według popularności. Najedź wskaźnikiem "
"nad nazwę strony aby zobaczyć liczbę odwiedzin."

#: ../../stattrans.pl:630
msgid "Outdated translations"
msgstr "Zdezaktualizowane tłumaczenia"

#: ../../stattrans.pl:632 ../../stattrans.pl:686
msgid "File"
msgstr "Plik"

#: ../../stattrans.pl:634
msgid "Diff"
msgstr "Diff"

#: ../../stattrans.pl:636
msgid "Comment"
msgstr "Komentarz"

#: ../../stattrans.pl:637
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:638
msgid "Git command line"
msgstr ""

#: ../../stattrans.pl:640
msgid "Log"
msgstr "Dziennik"

#: ../../stattrans.pl:641
msgid "Translation"
msgstr "Tłumaczenie"

#: ../../stattrans.pl:642
msgid "Maintainer"
msgstr "Opiekun"

#: ../../stattrans.pl:644
msgid "Status"
msgstr "Stan"

#: ../../stattrans.pl:645
msgid "Translator"
msgstr "Tłumacz"

#: ../../stattrans.pl:646
msgid "Date"
msgstr "Data"

#: ../../stattrans.pl:653
msgid "General pages not translated"
msgstr "Nie przetłumaczone strony ogólne"

#: ../../stattrans.pl:654
msgid "Untranslated general pages"
msgstr "Przetłumaczone strony ogólne"

#: ../../stattrans.pl:659
msgid "News items not translated"
msgstr "Nieprzetłumaczone wiadomości"

#: ../../stattrans.pl:660
msgid "Untranslated news items"
msgstr "Nieprzetłumaczone wiadomości"

#: ../../stattrans.pl:665
msgid "Consultant/user pages not translated"
msgstr "Nieprzetłumaczone strony konsultantów/użytkowników"

#: ../../stattrans.pl:666
msgid "Untranslated consultant/user pages"
msgstr "Nieprzetłumaczone strony konsultantów/użytkowników"

#: ../../stattrans.pl:671
msgid "International pages not translated"
msgstr "Nieprzetłumaczone strony poszczególnych języków"

#: ../../stattrans.pl:672
msgid "Untranslated international pages"
msgstr "Nieprzetłumaczone strony poszczególnych języków"

#: ../../stattrans.pl:677
msgid "Translated pages (up-to-date)"
msgstr "Przetłumaczone strony (aktualne)"

#: ../../stattrans.pl:684 ../../stattrans.pl:834
msgid "Translated templates (PO files)"
msgstr "Przetłumaczone szablony (pliki PO)"

#: ../../stattrans.pl:685 ../../stattrans.pl:837
msgid "PO Translation Statistics"
msgstr "Statystyki tłumaczeń PO"

#: ../../stattrans.pl:688 ../../stattrans.pl:851
msgid "Fuzzy"
msgstr "Fuzzy"

#: ../../stattrans.pl:689
msgid "Untranslated"
msgstr "Nieprzetłumaczone"

#: ../../stattrans.pl:690
msgid "Total"
msgstr "Łącznie"

#: ../../stattrans.pl:707
msgid "Total:"
msgstr "Łącznie:"

#: ../../stattrans.pl:741
msgid "Translated web pages"
msgstr "Przetłumaczone strony"

#: ../../stattrans.pl:744
msgid "Translation Statistics by Page Count"
msgstr "Statystyki tłumaczeń według ilości stron"

#: ../../stattrans.pl:759 ../../stattrans.pl:805 ../../stattrans.pl:849
msgid "Language"
msgstr "Język"

#: ../../stattrans.pl:760 ../../stattrans.pl:806
msgid "Translations"
msgstr "Tłumaczenia"

#: ../../stattrans.pl:787
msgid "Translated web pages (by size)"
msgstr "Przetłumaczone strony (według rozmiaru)"

#: ../../stattrans.pl:790
msgid "Translation Statistics by Page Size"
msgstr "Statystyki tłumaczeń według wielkości strony"

#~ msgid "Created with"
#~ msgstr "Utworzone przy pomocy"

#, fuzzy
#~| msgid "Colored diff"
#~ msgid "Commit diff"
#~ msgstr "Kolorowy diff"

#~ msgid "Colored diff"
#~ msgstr "Kolorowy diff"

#~ msgid "Unified diff"
#~ msgstr "Połączony diff"
