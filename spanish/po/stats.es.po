# Copyright (C) 2012 Software in the Public Interest, SPI Inc.
#
# Changes:
# - Initial translation
#      Fernando C. Estrada <fcestrada@fcestrada.com> , 2012
#
#  Traductores, si no conoce el formato PO, merece la pena leer la
#  documentación de gettext, especialmente las secciones dedicadas a este
#  formato, por ejemplo ejecutando:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
# Equipo de traducción al español, por favor lean antes de traducir
# los siguientes documentos:
#
# - El proyecto de traducción de Debian al español
#   https://www.debian.org/intl/spanish/
#   especialmente las notas y normas de traducción en
#   https://www.debian.org/intl/spanish/notas
#
# - La guía de traducción de po's de debconf:
#   /usr/share/doc/po-debconf/README-trans
#   o https://www.debian.org/intl/l10n/po-debconf/README-trans
#
# Si tiene dudas o consultas sobre esta traducción consulte con el último
# traductor (campo Last-Translator) y ponga en copia a la lista de
# traducción de Debian al español (<debian-l10n-spanish@lists.debian.org>)
#
msgid ""
msgstr ""
"Project-Id-Version: Debian Website\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2018-06-09 18:51+0200\n"
"Last-Translator: Laura Arjona Reina <larjona@debian.org>\n"
"Language-Team: Debian L10N Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.8.11\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Estadísticas de traducción del sitio web de Debian"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Hay %d páginas por traducir."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Hay %d bytes por traducir."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Hay %d cadenas de caracteres por traducir."

#: ../../stattrans.pl:278 ../../stattrans.pl:494
msgid "Wrong translation version"
msgstr "Versión incorrecta de traducción"

#: ../../stattrans.pl:280
msgid "This translation is too out of date"
msgstr "Esta traducción está muy desactualizada"

#: ../../stattrans.pl:282
msgid "The original is newer than this translation"
msgstr "La original es más nueva que esta traducción"

#: ../../stattrans.pl:286 ../../stattrans.pl:494
msgid "The original no longer exists"
msgstr "La original ya no existe"

#: ../../stattrans.pl:470
msgid "hit count N/A"
msgstr "contador de visitas N/A"

#: ../../stattrans.pl:470
msgid "hits"
msgstr "visitas"

#: ../../stattrans.pl:488 ../../stattrans.pl:489
msgid "Click to fetch diffstat data"
msgstr "Haga clic para obtener la información de diffstat"

#: ../../stattrans.pl:599 ../../stattrans.pl:739
msgid "Created with <transstatslink>"
msgstr "Creado con <transstatslink>"

#: ../../stattrans.pl:604
msgid "Translation summary for"
msgstr "Resumen de traducción para"

#: ../../stattrans.pl:607 ../../stattrans.pl:763 ../../stattrans.pl:809
#: ../../stattrans.pl:852
msgid "Not translated"
msgstr "Sin traducir"

#: ../../stattrans.pl:607 ../../stattrans.pl:762 ../../stattrans.pl:808
msgid "Outdated"
msgstr "Desactualizado"

#: ../../stattrans.pl:607
msgid "Translated"
msgstr "Traducido"

#: ../../stattrans.pl:607 ../../stattrans.pl:687 ../../stattrans.pl:761
#: ../../stattrans.pl:807 ../../stattrans.pl:850
msgid "Up to date"
msgstr "Actualizado"

#: ../../stattrans.pl:608 ../../stattrans.pl:609 ../../stattrans.pl:610
#: ../../stattrans.pl:611
msgid "files"
msgstr "ficheros"

#: ../../stattrans.pl:614 ../../stattrans.pl:615 ../../stattrans.pl:616
#: ../../stattrans.pl:617
msgid "bytes"
msgstr "bytes"

#: ../../stattrans.pl:624
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Nota: el listado de páginas está ordenado por popularidad. Coloque el ratón "
"sobre el nombre de la página para ver el número de visitas."

#: ../../stattrans.pl:630
msgid "Outdated translations"
msgstr "Traducciones desactualizadas"

#: ../../stattrans.pl:632 ../../stattrans.pl:686
msgid "File"
msgstr "Fichero"

#: ../../stattrans.pl:634
msgid "Diff"
msgstr "Diferencias"

#: ../../stattrans.pl:636
msgid "Comment"
msgstr "Comentarios"

#: ../../stattrans.pl:637
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:638
msgid "Git command line"
msgstr "línea de orden «git»"

#: ../../stattrans.pl:640
msgid "Log"
msgstr "Registro"

#: ../../stattrans.pl:641
msgid "Translation"
msgstr "Traducción"

#: ../../stattrans.pl:642
msgid "Maintainer"
msgstr "Encargado"

#: ../../stattrans.pl:644
msgid "Status"
msgstr "Estado"

#: ../../stattrans.pl:645
msgid "Translator"
msgstr "Traductor"

#: ../../stattrans.pl:646
msgid "Date"
msgstr "Fecha"

#: ../../stattrans.pl:653
msgid "General pages not translated"
msgstr "Páginas generales no traducidas"

#: ../../stattrans.pl:654
msgid "Untranslated general pages"
msgstr "Páginas generales sin traducir"

#: ../../stattrans.pl:659
msgid "News items not translated"
msgstr "Elementos de noticias no traducidos"

#: ../../stattrans.pl:660
msgid "Untranslated news items"
msgstr "Elementos de noticias sin traducir"

#: ../../stattrans.pl:665
msgid "Consultant/user pages not translated"
msgstr "Páginas de consultores/usuarios no traducidas"

#: ../../stattrans.pl:666
msgid "Untranslated consultant/user pages"
msgstr "Páginas de consultores/usuarios sin traducir"

#: ../../stattrans.pl:671
msgid "International pages not translated"
msgstr "Páginas internacionales no traducidas"

#: ../../stattrans.pl:672
msgid "Untranslated international pages"
msgstr "Páginas internacionales sin traducir"

#: ../../stattrans.pl:677
msgid "Translated pages (up-to-date)"
msgstr "Páginas traducidas (actualizadas)"

#: ../../stattrans.pl:684 ../../stattrans.pl:834
msgid "Translated templates (PO files)"
msgstr "Plantillas traducidas (ficheros PO)"

#: ../../stattrans.pl:685 ../../stattrans.pl:837
msgid "PO Translation Statistics"
msgstr "Estadísticas de traducción de ficheros PO"

#: ../../stattrans.pl:688 ../../stattrans.pl:851
msgid "Fuzzy"
msgstr "Impreciso"

#: ../../stattrans.pl:689
msgid "Untranslated"
msgstr "Sin traducir"

#: ../../stattrans.pl:690
msgid "Total"
msgstr "Total"

#: ../../stattrans.pl:707
msgid "Total:"
msgstr "Total:"

#: ../../stattrans.pl:741
msgid "Translated web pages"
msgstr "Páginas web traducidas"

#: ../../stattrans.pl:744
msgid "Translation Statistics by Page Count"
msgstr "Estadísticas de traducción por número de páginas"

#: ../../stattrans.pl:759 ../../stattrans.pl:805 ../../stattrans.pl:849
msgid "Language"
msgstr "Idioma"

#: ../../stattrans.pl:760 ../../stattrans.pl:806
msgid "Translations"
msgstr "Traducciones"

#: ../../stattrans.pl:787
msgid "Translated web pages (by size)"
msgstr "Páginas web traducidas (por tamaño)"

#: ../../stattrans.pl:790
msgid "Translation Statistics by Page Size"
msgstr "Estadísticas de traducción por tamaño de las páginas"

#~ msgid "Unified diff"
#~ msgstr "Diff unificado"

#~ msgid "Colored diff"
#~ msgstr "Diff coloreado"

#~ msgid "Commit diff"
#~ msgstr "Diff de los cambios"

#~ msgid "Created with"
#~ msgstr "Creado por"
