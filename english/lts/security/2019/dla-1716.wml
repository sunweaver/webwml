<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The ikiwiki maintainers discovered that the aggregate plugin did not use
LWPx::ParanoidAgent. On sites where the aggregate plugin is enabled, authorized
wiki editors could tell ikiwiki to fetch potentially undesired URIs even if
LWPx::ParanoidAgent was installed:</p>

    <p>local files via file: URIs
    other URI schemes that might be misused by attackers, such as gopher:
    hosts that resolve to loopback IP addresses (127.x.x.x)
    hosts that resolve to RFC 1918 IP addresses (192.168.x.x etc.)</p>

<p>This could be used by an attacker to publish information that should not have
been accessible, cause denial of service by requesting <q>tarpit</q> URIs that are
slow to respond, or cause undesired side-effects if local web servers implement
<q>unsafe</q> GET requests. (<a href="https://security-tracker.debian.org/tracker/CVE-2019-9187">CVE-2019-9187</a>)</p>

<p>Additionally, if liblwpx-paranoidagent-perl is not installed, the
blogspam, openid and pinger plugins would fall back to LWP, which is
susceptible to similar attacks. This is unlikely to be a practical problem for
the blogspam plugin because the URL it requests is under the control of the
wiki administrator, but the openid plugin can request URLs controlled by
unauthenticated remote users, and the pinger plugin can request URLs controlled
by authorized wiki editors.</p>

<p>This is addressed in ikiwiki 3.20190228 as follows, with the same fixes
backported to Debian 9 in version 3.20170111.1:</p>

<ul>

<li>URI schemes other than http: and https: are not accepted, preventing access
  to file:, gopher:, etc.</li>

<li>If a proxy is configured in the ikiwiki setup file, it is used for all
  outgoing http: and https: requests. In this case the proxy is responsible for
  blocking any requests that are undesired, including loopback or RFC 1918
  addresses.</li>

<li>If a proxy is not configured, and liblwpx-paranoidagent-perl is installed, it
  will be used. This prevents loopback and RFC 1918 IP addresses, and sets a
  timeout to avoid denial of service via <q>tarpit</q> URIs.</li>

<li>Otherwise, the ordinary LWP user-agent will be used. This allows requests to
  loopback and RFC 1918 IP addresses, and has less robust timeout behaviour.
  We are not treating this as a vulnerability: if this behaviour is not
  acceptable for your site, please make sure to install LWPx::ParanoidAgent or
  disable the affected plugins.</li>

</ul>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.20141016.4+deb8u1.</p>

<p>We recommend that you upgrade your ikiwiki packages. In addition it is also
recommended that you have liblwpx-paranoidagent-perl installed, which listed in
the recommends field of ikiwiki.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1716.data"
# $Id: $
