<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A regression was found in the recent security update for 389-ds-base
(the 389 Directory Server), announced as DLA-1554-2, caused by an
incomplete fix for <a href="https://security-tracker.debian.org/tracker/CVE-2018-14648">CVE-2018-14648</a>. 
The regression caused the server
to crash when processing requests with empty attributes.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.3.3.5-4+deb8u5.</p>

<p>We recommend that you upgrade your 389-ds-base packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1554-2.data"
# $Id: $
