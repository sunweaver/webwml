<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Michal Kedzior found two vulnerabilities in LDAP Account Manager, a web
front-end for LDAP directories.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8763">CVE-2018-8763</a>

    <p>The found Reflected Cross Site Scripting (XSS) vulnerability might
    allow an attacker to execute JavaScript code in the browser of the
    victim or to redirect her to a malicious website if the victim clicks
    on a specially crafted link.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.7-2+deb7u1.</p>

<p>We recommend that you upgrade your ldap-account-manager packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1342.data"
# $Id: $
