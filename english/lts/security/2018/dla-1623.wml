<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there was a potential denial of service
vulnerability in tar, the GNU version of the tar UNIX archiving
utility.</p>

<p>The --sparse argument looped endlessly if the file shrank whilst
it was being read. Tar would only break out of this endless loop
if the file grew again to (or beyond) its original end of file.</p>

<p>For Debian 8 <q>Jessie</q>, this issue has been fixed in tar version
1.27.1-2+deb8u2.</p>

<p>We recommend that you upgrade your tar packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1623.data"
# $Id: $
