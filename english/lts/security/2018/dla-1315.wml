<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Daniel P. Berrange and Peter Krempa of Red Hat discovered a flaw in
libvirt, a virtualization API. A lack of restriction for the amount of
data read by QEMU Monitor socket can lead to a denial of service by
exhaustion of memory resources.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.9.12.3-1+deb7u3.</p>

<p>We recommend that you upgrade your libvirt packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1315.data"
# $Id: $
