<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8615">CVE-2016-8615</a>

      <p>If cookie state is written into a cookie jar file that is later read
      back and used for subsequent requests, a malicious HTTP server can
      inject new cookies for arbitrary domains into said cookie jar.
      The issue pertains to the function that loads cookies into memory, which
      reads the specified file into a fixed-size buffer in a line-by-line
      manner using the `fgets()` function. If an invocation of fgets() cannot
      read the whole line into the destination buffer due to it being too
      small, it truncates the output.
      This way, a very long cookie (name + value) sent by a malicious server
      would be stored in the file and subsequently that cookie could be read
      partially and crafted correctly, it could be treated as a different
      cookie for another server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8616">CVE-2016-8616</a>

      <p>When re-using a connection, curl was doing case insensitive comparisons
      of user name and password with the existing connections.
      This means that if an unused connection with proper credentials exists
      for a protocol that has connection-scoped credentials, an attacker can
      cause that connection to be reused if s/he knows the case-insensitive
      version of the correct password.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8617">CVE-2016-8617</a>

      <p>In libcurl's base64 encode function, the output buffer is allocated
      as follows without any checks on insize:
         malloc( insize * 4 / 3 + 4 )
      On systems with 32-bit addresses in userspace (e.g. x86, ARM, x32),
      the multiplication in the expression wraps around if insize is at
      least 1GB of data. If this happens, an undersized output buffer will
      be allocated, but the full result will be written, thus causing the
      memory behind the output buffer to be overwritten.
      Systems with 64 bit versions of the `size_t` type are not affected
      by this issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8618">CVE-2016-8618</a>

      <p>The libcurl API function called `curl_maprintf()` can be tricked into
      doing a double-free due to an unsafe `size_t` multiplication, on
      systems using 32 bit `size_t` variables. The function is also used
      internallty in numerous situations.
      Systems with 64 bit versions of the `size_t` type are not affected
      by this issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8619">CVE-2016-8619</a>

      <p>In curl's implementation of the Kerberos authentication mechanism,
      the function `read_data()` in security.c is used to fill the
      necessary krb5 structures. When reading one of the length fields from
      the socket, it fails to ensure that the length parameter passed to
      realloc() is not set to 0.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8621">CVE-2016-8621</a>

      <p>The `curl_getdate` converts a given date string into a numerical
      timestamp and it supports a range of different formats and
      possibilites to express a date and time. The underlying date
      parsing function is also used internally when parsing for example
      HTTP cookies (possibly received from remote servers) and it can be
      used when doing conditional HTTP requests.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8622">CVE-2016-8622</a>

      <p>The URL percent-encoding decode function in libcurl is called
      `curl_easy_unescape`. Internally, even if this function would be
      made to allocate a unscape destination buffer larger than 2GB, it
      would return that new length in a signed 32 bit integer variable,
      thus the length would get either just truncated or both truncated
      and turned negative. That could then lead to libcurl writing outside
      of its heap based buffer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8623">CVE-2016-8623</a>

      <p>libcurl explicitly allows users to share cookies between multiple
      easy handles that are concurrently employed by different threads.
      When cookies to be sent to a server are collected, the matching
      function collects all cookies to send and the cookie lock is released
      immediately afterwards. That funcion however only returns a list with
      *references* back to the original strings for name, value, path and so
      on. Therefore, if another thread quickly takes the lock and frees one
      of the original cookie structs together with its strings,
      a use-after-free can occur and lead to information disclosure. Another
      thread can also replace the contents of the cookies from separate HTTP
      responses or API calls.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8624">CVE-2016-8624</a>

      <p>curl doesn't parse the authority component of the URL correctly when
      the host name part ends with a '#' character, and could instead be
      tricked into connecting to a different host. This may have security
      implications if you for example use an URL parser that follows the RFC
      to check for allowed domains before using curl to request them.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
7.26.0-1+wheezy17.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-711.data"
# $Id: $
