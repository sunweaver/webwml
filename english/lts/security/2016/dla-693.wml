<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The libtiff library and associated tools provided in libtiff-tools are
vulnerable to many security problems.</p>

<p>This update drops many tools which are no longer supported upstream
and which are affected by multiple memory corruption issues:</p>

<ul>
<li>bmp2tiff (<a href="https://security-tracker.debian.org/tracker/CVE-2016-3619">CVE-2016-3619</a>,
	    <a href="https://security-tracker.debian.org/tracker/CVE-2016-3620">CVE-2016-3620</a>,
	    <a href="https://security-tracker.debian.org/tracker/CVE-2016-3621">CVE-2016-3621</a>,
	    <a href="https://security-tracker.debian.org/tracker/CVE-2016-5319">CVE-2016-5319</a>,
            <a href="https://security-tracker.debian.org/tracker/CVE-2015-8668">CVE-2015-8668</a>)</li>
<li>gif2tiff (<a href="https://security-tracker.debian.org/tracker/CVE-2016-3186">CVE-2016-3186</a>,
	      <a href="https://security-tracker.debian.org/tracker/CVE-2016-5102">CVE-2016-5102</a>)</li>
<li>ras2tiff</li>
<li>sgi2tiff</li>
<li>sgisv</li>
<li>ycbcr</li>
<li>rgb2ycbcr (<a href="https://security-tracker.debian.org/tracker/CVE-2016-3623">CVE-2016-3623</a>,
	       <a href="https://security-tracker.debian.org/tracker/CVE-2016-3624">CVE-2016-3624</a>)</li>
<li>thumbnail (<a href="https://security-tracker.debian.org/tracker/CVE-2016-3631">CVE-2016-3631</a>,
	       <a href="https://security-tracker.debian.org/tracker/CVE-2016-3632">CVE-2016-3632</a>,
	       <a href="https://security-tracker.debian.org/tracker/CVE-2016-3633">CVE-2016-3633</a>,
	       <a href="https://security-tracker.debian.org/tracker/CVE-2016-3634">CVE-2016-3634</a>,
               <a href="https://security-tracker.debian.org/tracker/CVE-2016-8331">CVE-2016-8331</a>)</li>
</ul>

<p>This update also fixes the following issues:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-8128">CVE-2014-8128</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2015-7554">CVE-2015-7554</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-5318">CVE-2016-5318</a>

    <p>Multiple buffer overflows triggered through TIFFGetField() on unknown
    tags. Lacking an upstream fix, the list of known tags has been
    extended to cover all those that are in use by the TIFF tools.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5652">CVE-2016-5652</a>

    <p>Heap based buffer overflow in tiff2pdf.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6223">CVE-2016-6223</a>

    <p>Information leak in libtiff/tif_read.c. Fix out-of-bounds read on
    memory-mapped files in TIFFReadRawStrip1() and TIFFReadRawTile1()
    when stripoffset is beyond tmsize_t max value (reported by
    Mathias Svensson).</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.0.2-6+deb7u7.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-693.data"
# $Id: $
