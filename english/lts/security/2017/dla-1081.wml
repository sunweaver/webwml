<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This updates fixes numerous vulnerabilities in imagemagick: Various
memory handling problems and cases of missing or incomplete input
sanitising may result in denial of service, memory disclosure or the
execution of arbitrary code if malformed DPX, RLE, CIN, DIB, EPT, MAT,
VST, PNG, JNG, MNG, DVJU, JPEG, TXT, PES, MPC, UIL, PS, PALM, CIP, TIFF,
ICON, MAGICK, DCM, MSL, WMF, MIFF, PCX, SUN, PSD, MVG, PWP, PICT, PDB,
SFW, or XCF files are processed.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
6.7.7.10-5+deb7u16.</p>

<p>We recommend that you upgrade your imagemagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1081.data"
# $Id: $
