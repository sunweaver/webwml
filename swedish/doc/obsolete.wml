#use wml::debian::template title="Förlegad dokumentation"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#use wml::debian::translation-check translation="2d40937dc178970beff469909758a1485fd4c1d2"

<h1 id="historical">Historiska dokument</h1>

<p>De dokument som listas nedan skrevs antingen för väldigt länge sedan och
är inte uppdaterade eller har skrivits för tidigare versioner av Debian
och har inte uppdaterats till aktuella versioner. Information i dessa dokument är
gammal, men kan ändå vara av intresse för någon.</p>

<p>De dokument som har förlorat sin relevans och inte har något syfte längre
har fått sina referenser borttagna, men källkoden för dessa föråldrade manualer
finns fortfarande på
<a href="https://salsa.debian.org/ddp-team/attic">DDP's vind</a>.</p>


<h2 id="user">Användaroriented dokumentation</h2>

<document "dselect-dokumentation för nybörjare" "dselect">

<div class="centerblock">
<p>
  Denna fil dokumenterar dselect för förstagångsanvändare och är tänkt som en hjälp
  till att installera Debian framgångsrikt. Den försöker inte förklara
  allt, så när du möter dselect för första gången, arbeta med hjälp av hjälpskärmarna.
</p>
<doctable>
  <authors "St&eacute;phane Bortzmeyer">
  <maintainer "(?)">
  <status>
  avstannad: <a href="https://packages.debian.org/aptitude">aptitude</a> har
  ersatt dselect som Debians standardpakethanteringsgränssnitt
  </status>
  <availability>
  <inddpcvs name="dselect-beginner" formats="html txt pdf ps"
            langs="ca cs da de en es fr hr it ja pl pt ru sk">
  </availability>
</doctable>
</div>

<hr />

<document "Användarguide" "users-guide">

<div class="centerblock">
<p>
Denna <q>Användarguide</q> är inget annat än en omformatterad <q>Användarguide för Progeny</q>.
Innehållet har anpassats till Debians standardsystem.</p>

<p>Över 300 sidor med en bra genomgång kring hur man börjar använda Debiansystemet
från <acronym lang="en" title="Graphical User Interface">GUI</acronym>-skrivbord
och skalkommandoraden.
</p>
<doctable>
  <authors "Progeny Linux Systems, Inc.">
  <maintainer "Osamu Aoki (&#38738;&#26408; &#20462;)">
  <status>
  Användbar som en genomgång.  Skriven för Woody-utgåvan,
  blir alltmer utdaterad.
  </status>
  <availability>
# langs="en" isn't redundant, it adds the needed language suffix to the link
  <inddpcvs name="users-guide" index="users-guide" langs="en" formats="html txt pdf">
  </availability>
</doctable>
</div>

<hr />

<document "Debiangenomgång" "tutorial">

<div class="centerblock">
<p>
Denna manual är riktad till en ny Linuxanvändare, för att hjälpa denna att bekanta sig
med Linux efter att de har installerat det, eller till en ny Linuxanvändare på ett system
som någon annan administrerar.
</p>
<doctable>
  <authors "Havoc Pennington, Oliver Elphick, Ole Tetlie, James Treacy,
  Craig Sawyer, Ivan E. Moore II">
  <editors "Havoc Pennington">
  <maintainer "(?)">
  <status>
  avstannad; ofullständig;
  förmodligen ersatt av <a href="user-manuals#quick-reference">Debianreferens</a>
  </status>
  <availability>
  inte komplett
  <inddpcvs name="debian-tutorial" cvsname="tutorial">
  </availability>
</doctable>
</div>

<hr />

<document "Debian GNU/Linux: Guide till installation och användning" "guide">

<div class="centerblock">
<p>
  En manual riktad till slutanvändaren.
</p>
<doctable>
  <authors "John Goerzen, Ossama Othman">
  <editors "John Goerzen">
  <status>
  färdig (men för potato)
  </status>
  <availability>
  <inoldpackage "debian-guide">
  </availability>
</doctable>
</div>

<hr />

<document "Debians användarreferensmanual" "userref">

<div class="centerblock">
<p>
  Denna manual tillhandahåller åtminstone en överblick kring allt en användare bör veta
  om sitt Debian GNU/Linux-system (d.v.s. sätta upp X, hur man konfigurerar
  nätverket, kommer åt disketter, etc.). Det är menat att fylla glappet
  mellan Debiangenomgången och de detaljerade manual- och infosidor
  som följer med varje paket.</p>

  <p>Den är också menad att ge en idé om hur man kombinerar kommandon, i linje med
  den generella Unixprincipen att <em>det finns alltid mer än ett sätt att
  göra det</em>.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Jason D. Waterman, Havoc Pennington,
      Oliver Elphick, Bruce Evry, Karl-Heinz Zimmer">
  <editors "Thalia L. Hooker, Oliver Elphick">
  <maintainer "(?)">
  <status>
  avstannad och ganska ofullständig;
  förmodligen utdaterad av <a href="user-manuals#quick-reference">Debianreferensen</a>
  </status>
  <availability>
  <inddpcvs name="user">
  </availability>
</doctable>
</div>

<hr />


<document "Debiansystemadministratörsmanual" "system">

<div class="centerblock">
<p>
  Detta dokument nämns i introduktionen till policymanualen.
  Det täcker alla systemadministrationsaspekter av ett Debiansystem.
</p>
<doctable>
  <authors "Tapio Lehtonen">
  <maintainer "(?)">
  <status>
  avstannad; ofullständig;
  förmodligen utdaterad av <a href="user-manuals#quick-reference">Debianreferensen</a>
  </status>
  <availability>
  ännu inte tillgänglig
  <inddpcvs name="system-administrator">
  </availability>
</doctable>
</div>

<hr />

<document "Debiannätverksadministratörsmanual" "network">

<div class="centerblock">
<p>
  Denna manual täcker alla nätverksadministrationsaspekter av ett Debiansystem.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Oliver Elphick, Duncan C. Thomson, Ivan E. Moore II">
  <maintainer "(?)">
  <status>
  avstannad; ofullständig;
  förmodligen utdaterad av <a href="user-manuals#quick-reference">Debianreferensen</a>
  </status>
  <availability>
  ännu inte tillgänglig
  <inddpcvs name="network-administrator">
  </availability>
</doctable>
</div>

<hr />

# Add this to books, there's a revised edition (2nd) @ Amazon
<document "The Linux Cookbook" "linuxcookbook">

<div class="centerblock">
<p>
  En referensguide till Debian GNU/Linux-systemet som visar,
  med hjälp av fler än 1500 <q>recept</q>, hur man använder det för allehanda daliga aktiviteter &ndash; från
  arbete med text, bilder och ljud till produktivitets- och nätverksfrågor.
  Precis som den mjukvara boken beskriver, är den <q>copyleft</q>
  och dess källkod finns tillgänglig.
</p>
<doctable>
  <authors "Michael Stutz">
  <status>
  publicerad; skriven för woody, börjar bli utdaterad
  </status>
  <availability>
  <inoldpackage "linuxcookbook">
  <p><a href="http://dsl.org/cookbook/">från författaren</a>
  </availability>
</doctable>
</div>

<document "APT HOWTO" "apt-howto">

<div class="centerblock">
<p>
  Denna manual försöker vara en snabb men komplett informationskälla
  om APT-systemet och dess funktioner. Den innehåller mycket information
  om huvudanvändingsområdena för APT och flera exempel.
</p>
<doctable>
  <authors "Gustavo Noronha Silva">
  <maintainer "Gustavo Noronha Silva">
  <status>
  färdig
  </status>
  <availability>
  <inpackage "apt-howto">
  <inddpcvs name="apt-howto" langs="en ca cs de es el fr it ja pl pt-br ru uk tr zh-tw zh-cn"
	    formats="html txt pdf ps" naming="locale" />
  </availability>
</doctable>
</div>


<h2 id="devel">Utvecklardokumentation</h2>

<document "Introduktion: Att göra ett Debianpaket" "makeadeb">

<div class="centerblock">
<p>
  Introduktion om hur man gör en <code>.deb</code> med hjälp av
  <strong>debmake</strong>.
</p>
<doctable>
  <authors "Jaldhar H. Vyas">
  <status>
  avstannad, utdaterad av <a href="devel-manuals#maint-guide">Debian New Maintainers' Guide</a>
  </status>
  <availability>
  <a href="https://people.debian.org/~jaldhar/">HTML online</a>
  </availability>
</doctable>
</div>

<hr />

<document "Debianprogrammerarens manual" "programmers">

<div class="centerblock">
<p>
  Hjälper nya utvecklare att skapa ett paket för Debian GNU/Linux-systemet.
</p>
<doctable>
  <authors "Igor Grobman">
  <status>
  utdaterad av <a href="devel-manuals#maint-guide">New Maintainers' Guide</a>
  </status>
  <availability>
  <inddpcvs name="programmer">
  </availability>
</doctable>
</div>

<hr />

<document "Debians paketeringshandbok" "packman">

<div class="centerblock">
<p>
  Denna handbok beskriver de tekniska aspekterna vad gäller att skapa
  binär- och källkodspaket för Debian.
  Den beskriver även gränssnittet mellan dselect och dess åtkomstskript.
  Den hanterar inte Debianprojektets policykrav, och förutsätter
  förtrogenhet med dpkgs funktioner från systemadministratörens synvinkel.

<doctable>
  <authors "Ian Jackson, Klee Dienes, David A. Morris, Christian Schwarz">
  <status>
  Delar som var de facto-policy flyttades nyligen in i 
  <a href="devel-manuals#policy">debian-policy</a>.
  </status>
  <availability>
    <inoldpackage "packaging-manual">
  </availability>
</doctable>
</div>

<h2 id="misc">Blandad dokumentation</h2>

<document "Guide till Debianarkiv" "repo">

<div class="centerblock">
<p>
  Detta dokument förklarar hur Debianarkiv fungerar, hur man skapar dem,
  samt hur man lägger dem till <tt>sources.list</tt> på rätt sätt.
</p>
<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  klar (?)
  </status>
  <availability>
  <inddpcvs name="repository-howto" index="repository-howto"
            formats="html" langs="en fr de uk ta">
  </availability>
</doctable>
</div>
