#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une augmentation de droits, un déni de service ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6554">CVE-2018-6554</a>

<p>Une fuite de mémoire dans la fonction irda_bind dans le sous-système irda a
été découverte. Un utilisateur local peut exploiter ce défaut pour provoquer un
déni de service (consommation de mémoire).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6555">CVE-2018-6555</a>

<p>Un défaut a été découvert dans la fonction irda_setsockopt dans le
sous-système irda, permettant à un utilisateur local de provoquer un déni de
service (utilisation de mémoire après libération et plantage du système).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7755">CVE-2018-7755</a>

<p>Brian Belleville a découvert un défaut dans la fonction fd_locked_ioctl
dans le pilote de disquette du noyau Linux. Le pilote copie un pointeur du noyau
pour utiliser de la mémoire d’utilisateur dans la  réponse à un ioctl FDGETPRM.
Un utilisateur local avec accès à un périphérique de disquette peut exploiter
ce défaut pour découvrir l’emplacement de code et données du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-9363">CVE-2018-9363</a>

<p>L’implémentation HIDP Bluetooth ne vérifiait pas correctement la longueur des
messages de rapport. Un périphérique HIDP appairé pourrait utiliser cela pour
provoquer un dépassement de tampon, aboutissant à un déni de service (corruption
de mémoire ou plantage) ou à une exécution potentielle de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-9516">CVE-2018-9516</a>

<p>L’interface d’évènements HID dans debugfs ne gérait pas correctement la limite
de longueur de copies pour utiliser des tampons. Un utilisateur local avec accès
à ces fichiers pourrait utiliser cela pour provoquer un déni de service
(corruption de mémoire ou plantage) ou éventuellement pour une augmentation de
droits. Cependant, par défaut debugfs est seulement accessible par le
superutilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10902">CVE-2018-10902</a>

<p>Le pilote rawmidi du noyau ne protégeait pas d’un accès concurrent qui menait
à un défaut de double réallocation (double libération de zone de mémoire). Un
attaquant local peut exploiter ce problème pour une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10938">CVE-2018-10938</a>

<p>Yves Younan de Cisco a signalé que le module Cipso IPv4 ne vérifiait
correctement la longueur des options IPv4. Avec des noyaux personnalisés et
CONFIG_NETLABEL activé, un attaquant distant pourrait utiliser cela pour
provoquer un déni de service (hang).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13099">CVE-2018-13099</a>

<p>Wen Xu de SSLab à Gatech a signalé un bogue d’utilisation de mémoire après
libération dans l’implémentation de F2FS. Un attaquant capable de monter un
volume F2FS contrefait pourrait utiliser cela pour provoquer un déni de service
(plantage ou corruption de mémoire) ou éventuellement pour une augmentation de
droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14609">CVE-2018-14609</a>

<p>Wen Xu de SSLab à Gatech a signalé un déréférencement potentiel de pointeur
NULL dans l’implémentation de F2FS. Un attaquant capable de monter des volumes
F2FS contrefaits pourrait utiliser cela pour provoquer un déni de service
(plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14617">CVE-2018-14617</a>

<p>Wen Xu de SSLab à Gatech a signalé un déréférencement potentiel de pointeur
 NULL dans l’implémentation de HFS+. Un attaquant capable de monter des volumes
HFS+ pourrait utiliser cela pour provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14633">CVE-2018-14633</a>

<p>Vincent Pelletier a découvert un défaut de dépassement de pile dans la
fonction chap_server_compute_md5() dans le code de la cible iSCSI. Un attaquant
distant non authentifié pourrait exploiter ce défaut pour provoquer un déni de
service ou éventuellement d’obtenir un accès non autorisé aux données exportées
par la cible iSCSI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14678">CVE-2018-14678</a>

<p>M. Vefa Bicakci et Andy Lutomirski ont découvert un défaut dans le code de
sortie du noyau utilisé sur les systèmes amd64 exécutés comme invités PV Xen.
Un utilisateur local pourrait utiliser cela pour provoquer un déni de service
(plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14734">CVE-2018-14734</a>

<p>Un bogue d’utilisation de mémoire après libération a été découvert dans le
gestionnaire de communications InfiniBand. Un utilisateur local pourrait utiliser
cela pour provoquer un déni de service (plantage ou corruption de mémoire) ou
pour une augmentation possible de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15572">CVE-2018-15572</a>

<p>Esmaiel Mohammadian Koruyeh, Khaled Khasawneh, Chengyu Song et Nael
Abu-Ghazaleh, de l’Université de Californie, Riverside, ont signalé une variante
de Spectre variant 2, appelée SpectreRSB. Un utilisateur local pourrait utiliser
cela pour lire des informations sensibles de processus possédés par d’autres
utilisateurs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15594">CVE-2018-15594</a>

<p>Nadav Amit a signalé que des appels indirects de fonction dans des invités
paravirtualisés étaient vulnérables à Spectre variant 2. Un utilisateur local
pourrait utiliser cela pour lire des informations sensibles du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16276">CVE-2018-16276</a>

<p>Jann Horn a découvert que le pilote yurex ne limitait pas correctement la
longueur des copies pour utiliser des tampons. Un utilisateur local avec accès
à un nœud de périphérique yurex pourrait utiliser cela pour provoquer un déni de
service (corruption de mémoire ou plantage) ou éventuellement pour une
augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16658">CVE-2018-16658</a>

<p>Le pilote de cdrom ne validait pas correctement le paramètre pour l’ioctl
CDROM_DRIVE_STATUS. Un utilisateur avec accès à un périphérique cdrom
pourrait utiliser cela pour lire des informations sensibles du noyau ou pour
provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17182">CVE-2018-17182</a>

<p>Jann Horn a découvert que la fonction vmacache_flush_all gère incorrectement
les dépassements de nombre de séquences. Un utilisateur local peut exploiter ce
défaut pour déclencher une utilisation de mémoire après libération, causant un
déni de service (plantage ou corruption de mémoire) ou une augmentation de
droits.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.9.110-3+deb9u5~deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets linux-4.9.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1531.data"
# $Id: $
