#use wml::debian::translation-check translation="244380ed6414a14f265795cff6ac8dab1d04e3a3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été corrigées dans la bibliothèque
multimédia libav qui pouvaient conduire à un déni de service, une divulgation
d'informations ou une exécution de code arbitraire si un fichier malformé était
traité.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9993">CVE-2017-9993</a>

<p>Libav ne restreint pas correctement les extensions de noms de fichier de flux
HTTP Live et de noms de démultiplexeur. Cela permet à des attaquants de lire des
fichiers arbitraires à l’aide de données de liste de lecture contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9994">CVE-2017-9994</a>

<p>libavcodec/webp.c dans Libav ne s’assure pas que pix_fmt est défini. Cela
permet à des attaquants distants de provoquer un déni de service (un dépassement
de tampon basé sur le tas et plantage d'application) ou éventuellement d’avoir
un impact non précisé à l'aide d'un fichier contrefait, relatif aux fonctions
vp8_decode_mb_row_no_filter et pred8x8_128_dc_8_cs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14055">CVE-2017-14055</a>

<p>Un déni de service dans mv_read_header() dû à une absence d’une vérification
d’EOF (End of File) peut provoquer une énorme consommation de CPU et mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14056">CVE-2017-14056</a>

<p>Un déni de service dans rl2_read_header() dû à une absence d’une vérification
d’EOF (End of File) peut provoquer une énorme consommation de CPU et mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14057">CVE-2017-14057</a>

<p>Un déni de service dans asf_read_marker() dû à une absence d’une vérification
d’EOF (End of File) peut provoquer une énorme consommation de CPU et mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14170">CVE-2017-14170</a>

<p>Un déni de service dans mxf_read_index_entry_array() dû à une absence d’une vérification
d’EOF (End of File) peut provoquer une énorme consommation de CPU et mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14171">CVE-2017-14171</a>

<p>Un déni de service dans nsv_parse_NSVf_header() dû à une absence d’une vérification
d’EOF (End of File) peut provoquer un énorme consommation de CPU et mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14767">CVE-2017-14767</a>

<p>La fonction sdp_parse_fmtp_config_h264 dans libavformat/rtpdec_h264.c gère
incorrectement les valeurs vides sprop-parameter-sets. Cela permet à des
attaquants distants de provoquer un déni de service (un dépassement de tampon
basé sur le tas) ou éventuellement d’avoir un impact non précisé à l'aide d'un
fichier sdp contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15672">CVE-2017-15672</a>

<p>La fonction read_header dans libavcodec/ffv1dec.c permet à des attaquants
distants d’avoir un impact non précisé à l'aide d'un fichier MP4 contrefait
déclenchant une lecture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17130">CVE-2017-17130</a>

<p>La fonction ff_free_picture_tables dans libavcodec/mpegpicture.c permet à des
attaquants distants de provoquer un déni de service (un dépassement de tampon
basé sur le tas et plantage d'application) ou éventuellement d’avoir un impact
non précisé à l'aide d'un fichier contrefait, relatif à
vc1_decode_i_blocks_adv.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6621">CVE-2018-6621</a>

<p>La fonction decode_frame dans libavcodec/utvideodec.c dans Libav permet à des
attaquants distants de provoquer un déni de service (lecture hors table) à
l'aide d'un fichier AVI contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7557">CVE-2018-7557</a>

<p>La fonction decode_init dans libavcodec/utvideodec.c dans Libav permet à des
attaquants distants de provoquer un déni de service (lecture hors table) à
l'aide d'un fichier AVI avec des dimensions contrefaites dans les données de
sous-échantillonnage chroma.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14394">CVE-2018-14394</a>

<p>libavformat/movenc.c dans Libav permet à des attaquants de provoquer un déni
de service (plantage d'application causé par une erreur de division par zéro)
avec un fichier audio Waveform contrefait d’utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1999010">CVE-2018-1999010</a>

<p>Libav contient plusieurs vulnérabilités d’accès hors table dans le protocole
mms qui peuvent aboutir à ce que des attaquants accèdent à des données hors
limites.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 6:11.12-1~deb8u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libav.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1630.data"
# $Id: $
