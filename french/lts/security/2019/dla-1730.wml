#use wml::debian::translation-check translation="ba57a248ba263b6695719c776e5da167539a570d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été récemment découverts dans libssh2, une
bibliothèque C coté client, mettant en œuvre le protocole SSH2.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3855">CVE-2019-3855</a>

<p>Un défaut de dépassement d'entier, qui pouvait conduire à une écriture hors
limites, a été découvert dans libssh2 dans la manière dont les paquets étaient
lus à partir du serveur. Un attaquant distant qui corrompait un serveur SSH,
pouvait exécuter du code dans le système client quand un utilisateur se
connectait sur le serveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3856">CVE-2019-3856</a>

<p>Un défaut de dépassement d'entier, qui pouvait conduire à une écriture hors
limites, a été découvert dans libssh2 dans la manière dont les requêtes de
saisie de clavier étaient analysées. Un attaquant distant qui corrompait un
serveur SSH, pouvait exécuter du code dans le système client quand un
utilisateur se connectait sur le serveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3857">CVE-2019-3857</a>

<p>Un défaut de dépassement d'entier, qui pouvait conduire à une écriture hors
limites, a été découvert dans libssh2 dans la manière dont les paquets
SSH_MSG_CHANNEL_REQUEST avec un signal exit étaient analysés. Un attaquant
distant qui corrompait un serveur SSH, pouvait exécuter du code dans le
système client quand un utilisateur se connectait sur le serveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3858">CVE-2019-3858</a>

<p>Un défaut de lecture hors limites a été découvert dans libssh2 quand un
paquet SFTP contrefait pour l'occasion était reçu du serveur. Un attaquant
distant qui corrompait un serveur SSH pouvait provoquer un déni de service
ou lire des données dans la mémoire du client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3859">CVE-2019-3859</a>

<p>Un défaut de lecture hors limites a été découvert dans les fonctions
_libssh2_paquet_require et _libssh2_paquet_requirev de libssh2. Un attaquant
distant qui corrompait un serveur SSH pouvait provoquer un déni de service
ou lire des données dans la mémoire du client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3860">CVE-2019-3860</a>

<p>Un défaut de lecture hors limites a été découvert dans libssh2 dans la
manière dont les paquets SFTP avec des charges vides étaient analysés. Un
attaquant distant qui corrompait un serveur SSH pouvait provoquer un déni de
service ou lire des données dans la mémoire du client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3861">CVE-2019-3861</a>

<p>Un défaut de lecture hors limites a été découvert dans libssh2 dans la
manière dont les paquets SSH avec une valeur de longueur de remplissage plus
grande que la longueur du paquet étaient analysés. Un attaquant distant qui
corrompait un serveur SSH pouvait provoquer un déni de service ou lire des
données dans la mémoire du client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3862">CVE-2019-3862</a>

<p>Un défaut de lecture hors limites a été découvert dans libssh2 dans la
manière dont les paquets SSH_MSG_CHANNEL_REQUEST avec un message d’état exit et
sans charge étaient analysés. Un attaquant distant qui corrompait un serveur
SSH pouvait provoquer un déni de service ou lire des données dans la mémoire
du client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3863">CVE-2019-3863</a>

<p>Un serveur pouvait envoyer plusieurs messages pour réponse interactive au
clavier dont la longueur totale était plus grande que les caractères unsigned
char max. Cette valeur était utilisée comme index pour copier la mémoire
provoquant une erreur d’écriture en mémoire hors limites.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.4.3-4.1+deb8u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets libssh2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1730.data"
# $Id: $
