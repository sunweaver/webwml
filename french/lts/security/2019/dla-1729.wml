#use wml::debian::translation-check translation="4155ba4afd4c2c3f60348343afc14026bbc78f4b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans wireshark, un analyseur de
trafic réseau.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9209">CVE-2019-9209</a>:

<p>Prévention du plantage du BER ASN.1 et des dissecteurs associés en évitant un
dépassement de tampon associé avec des nombres excessifs dans les valeurs de
temps.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9349">CVE-2017-9349</a>:

<p>Correction d’une boucle infinie dans le dissecteur DICOM par la validation
d’une valeur de longueur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9344">CVE-2017-9344</a>:

<p>Évitement d’une division par zéro, par la validation d’une valeur
d’intervalle dans le dissecteur Bluetooth L2CAP.</p>


<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.12.1+g01b65bf-4+deb8u18.</p>
<p>Nous vous recommandons de mettre à jour vos paquets wireshark.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1729.data"
# $Id: $
