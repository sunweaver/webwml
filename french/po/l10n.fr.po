msgid ""
msgstr ""
"Project-Id-Version: debian webwml l10n\n"
"PO-Revision-Date: 2008-11-26 23:40+0100\n"
"Last-Translator: French translation team <debian-l10n-french@lists.debian."
"org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/international/l10n/dtc.def:10
msgid "File"
msgstr "Fichier"

#: ../../english/international/l10n/dtc.def:14
msgid "Package"
msgstr "Paquet"

#: ../../english/international/l10n/dtc.def:18
msgid "Score"
msgstr "Note"

#: ../../english/international/l10n/dtc.def:22
msgid "Translator"
msgstr "Traducteur"

#: ../../english/international/l10n/dtc.def:26
msgid "Team"
msgstr "Équipe"

#: ../../english/international/l10n/dtc.def:30
msgid "Date"
msgstr "Date"

#: ../../english/international/l10n/dtc.def:34
msgid "Status"
msgstr "État"

#: ../../english/international/l10n/dtc.def:38
msgid "Strings"
msgstr "Chaînes"

#: ../../english/international/l10n/dtc.def:42
msgid "Bug"
msgstr "Bogue"

#: ../../english/international/l10n/dtc.def:49
msgid "<get-var lang />, as spoken in <get-var country />"
msgstr "<get-var lang /> &ndash; <get-var country />"

#: ../../english/international/l10n/dtc.def:54
msgid "Unknown language"
msgstr "langue inconnue"

#: ../../english/international/l10n/dtc.def:64
msgid "This page was generated with data collected on: <get-var date />."
msgstr ""
"Cette page a été produite à partir de données collectées le <get-var date />."

#: ../../english/international/l10n/dtc.def:69
msgid "Before working on these files, make sure they are up to date!"
msgstr ""
"Avant de travailler sur ces fichiers, vérifiez qu'ils sont à jour&nbsp;!"

#: ../../english/international/l10n/dtc.def:79
msgid "Section: <get-var name />"
msgstr "Section : <get-var name />"

#: ../../english/international/l10n/menu.def:10
msgid "L10n"
msgstr "Localisation"

#: ../../english/international/l10n/menu.def:14
msgid "Language list"
msgstr "Liste des langues"

#: ../../english/international/l10n/menu.def:18
msgid "Ranking"
msgstr "Classement"

#: ../../english/international/l10n/menu.def:22
msgid "Hints"
msgstr "Conseils"

#: ../../english/international/l10n/menu.def:26
msgid "Errors"
msgstr "Erreurs"

#: ../../english/international/l10n/menu.def:30
msgid "POT files"
msgstr "Fichiers POT"

#: ../../english/international/l10n/menu.def:34
msgid "Hints for translators"
msgstr "Conseils pratiques"
