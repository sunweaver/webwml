# Translation of stats.sk.po
# Copyright (C)
# This file is distributed under the same license as the webwml package.
# Ivan Masár <helix84@centrum.sk>, 2012, 2013.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"Report-Msgid-Bugs-To: debian-www@lists.debian.org\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2013-05-14 13:37+0200\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: x\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Štatistika prekladov webu Debianu"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "%d stránok na preloženie."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "%d bajtov na preloženie."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "%d reťazcov na preloženie."

#: ../../stattrans.pl:278 ../../stattrans.pl:494
msgid "Wrong translation version"
msgstr "Nesprávna verzia prekladu"

#: ../../stattrans.pl:280
msgid "This translation is too out of date"
msgstr "Tento preklad je príliš zastaraný"

#: ../../stattrans.pl:282
msgid "The original is newer than this translation"
msgstr "Originál je novší ako tento preklad"

#: ../../stattrans.pl:286 ../../stattrans.pl:494
msgid "The original no longer exists"
msgstr "Originál už neexistuje"

#: ../../stattrans.pl:470
msgid "hit count N/A"
msgstr "počet zásahov nedostupný"

#: ../../stattrans.pl:470
msgid "hits"
msgstr "zásahov"

#: ../../stattrans.pl:488 ../../stattrans.pl:489
msgid "Click to fetch diffstat data"
msgstr "Kliknutím stiahnete údaje diffstat"

#: ../../stattrans.pl:599 ../../stattrans.pl:739
msgid "Created with <transstatslink>"
msgstr ""

#: ../../stattrans.pl:604
msgid "Translation summary for"
msgstr "Zhrnutie pamäte prekladov pre"

#: ../../stattrans.pl:607 ../../stattrans.pl:763 ../../stattrans.pl:809
#: ../../stattrans.pl:852
msgid "Not translated"
msgstr "Nepreložené"

#: ../../stattrans.pl:607 ../../stattrans.pl:762 ../../stattrans.pl:808
msgid "Outdated"
msgstr "Zastarané"

#: ../../stattrans.pl:607
msgid "Translated"
msgstr "Preložené"

#: ../../stattrans.pl:607 ../../stattrans.pl:687 ../../stattrans.pl:761
#: ../../stattrans.pl:807 ../../stattrans.pl:850
msgid "Up to date"
msgstr "Aktuálne"

#: ../../stattrans.pl:608 ../../stattrans.pl:609 ../../stattrans.pl:610
#: ../../stattrans.pl:611
msgid "files"
msgstr "súborov"

#: ../../stattrans.pl:614 ../../stattrans.pl:615 ../../stattrans.pl:616
#: ../../stattrans.pl:617
msgid "bytes"
msgstr "bajtov"

#: ../../stattrans.pl:624
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Pozn.: zoznamy stránok sú zoradené podľa popularity. Ak podržíte myš nad "
"názvom stránky, uvidíte počet zásahov."

#: ../../stattrans.pl:630
msgid "Outdated translations"
msgstr "Zastarané preklady"

#: ../../stattrans.pl:632 ../../stattrans.pl:686
msgid "File"
msgstr "Súbor"

#: ../../stattrans.pl:634
msgid "Diff"
msgstr "Diff"

#: ../../stattrans.pl:636
msgid "Comment"
msgstr "Komentár"

#: ../../stattrans.pl:637
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:638
msgid "Git command line"
msgstr ""

#: ../../stattrans.pl:640
msgid "Log"
msgstr "Záznam"

#: ../../stattrans.pl:641
msgid "Translation"
msgstr "Preklad"

#: ../../stattrans.pl:642
msgid "Maintainer"
msgstr "Správca"

#: ../../stattrans.pl:644
msgid "Status"
msgstr "Stav"

#: ../../stattrans.pl:645
msgid "Translator"
msgstr "Prekladateľ"

#: ../../stattrans.pl:646
msgid "Date"
msgstr "Dátum"

#: ../../stattrans.pl:653
msgid "General pages not translated"
msgstr "Všeobecné stránky nepreložené"

#: ../../stattrans.pl:654
msgid "Untranslated general pages"
msgstr "Nepreložené všeobecné stránky"

#: ../../stattrans.pl:659
msgid "News items not translated"
msgstr "Novinky nepreložené"

#: ../../stattrans.pl:660
msgid "Untranslated news items"
msgstr "Nepreložené novinky"

#: ../../stattrans.pl:665
msgid "Consultant/user pages not translated"
msgstr "Stránky konzultantov používateľov nepreložené"

#: ../../stattrans.pl:666
msgid "Untranslated consultant/user pages"
msgstr "Nepreložené stránky konzultantov používateľov"

#: ../../stattrans.pl:671
msgid "International pages not translated"
msgstr "Medzinárodné stránky nepreložené"

#: ../../stattrans.pl:672
msgid "Untranslated international pages"
msgstr "Nepreložené medzinárodné stránky"

#: ../../stattrans.pl:677
msgid "Translated pages (up-to-date)"
msgstr "Preložené stránky (aktuálne)"

#: ../../stattrans.pl:684 ../../stattrans.pl:834
msgid "Translated templates (PO files)"
msgstr "Preložené šablóny (súbory .po)"

#: ../../stattrans.pl:685 ../../stattrans.pl:837
msgid "PO Translation Statistics"
msgstr "Štatistika prekladov .po"

#: ../../stattrans.pl:688 ../../stattrans.pl:851
msgid "Fuzzy"
msgstr "Fuzzy"

#: ../../stattrans.pl:689
msgid "Untranslated"
msgstr "Nepreložené"

#: ../../stattrans.pl:690
msgid "Total"
msgstr "Celkom"

#: ../../stattrans.pl:707
msgid "Total:"
msgstr "Celkom:"

#: ../../stattrans.pl:741
msgid "Translated web pages"
msgstr "Preložené webstránky"

#: ../../stattrans.pl:744
msgid "Translation Statistics by Page Count"
msgstr "Štatistika prekladov podľa počtu stránok"

#: ../../stattrans.pl:759 ../../stattrans.pl:805 ../../stattrans.pl:849
msgid "Language"
msgstr "Jazyk"

#: ../../stattrans.pl:760 ../../stattrans.pl:806
msgid "Translations"
msgstr "Preklady"

#: ../../stattrans.pl:787
msgid "Translated web pages (by size)"
msgstr "Preložené webstránky (podľa veľkosti)"

#: ../../stattrans.pl:790
msgid "Translation Statistics by Page Size"
msgstr "Štatistika prekladov podľa veľkosti stránky"

#~ msgid "Created with"
#~ msgstr "Vytvorené pomocou"

#, fuzzy
#~| msgid "Colored diff"
#~ msgid "Commit diff"
#~ msgstr "Zafarbený diff"

#~ msgid "Colored diff"
#~ msgstr "Zafarbený diff"

#~ msgid "Unified diff"
#~ msgstr "Zjednotený diff"
