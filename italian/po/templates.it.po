# Debian Web Site. templates.it.po
# Copyright (C) 2002 Giuseppe Sacco.
# Giuseppe Sacco <eppesuigoccas@libero.it>, 2002.
# Giuseppe Sacco <eppesuig@debian.org>, 2004-2007.
# Luca Monducci <luca.mo@tiscali.it>, 2008-2019.
# 
msgid ""
msgstr ""
"Project-Id-Version: templates.it\n"
"PO-Revision-Date: 2019-03-03 13:44+0100\n"
"Last-Translator: Luca Monducci <luca.mo@tiscali.it>\n"
"Language-Team: debian-l10n-italian <debian-l10n-italian@lists.debian.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/basic.wml:19
#: ../../english/template/debian/navbar.wml:11
msgid "Debian"
msgstr "Debian"

#: ../../english/template/debian/basic.wml:46
msgid "Debian website search"
msgstr "Cerca nel sito web Debian"

#: ../../english/template/debian/common_translation.wml:4
msgid "Yes"
msgstr "Sì"

#: ../../english/template/debian/common_translation.wml:7
msgid "No"
msgstr "No"

#: ../../english/template/debian/common_translation.wml:10
msgid "Debian Project"
msgstr "Progetto Debian"

#: ../../english/template/debian/common_translation.wml:13
msgid ""
"Debian is an operating system and a distribution of Free Software. It is "
"maintained and updated through the work of many users who volunteer their "
"time and effort."
msgstr ""
"Debian è un sistema operativo e una distribuzione di Software Libero. È "
"gestito e aggiornato grazie al lavoro di molti utenti che volontariamente "
"offrono il loro tempo e il loro lavoro."

#: ../../english/template/debian/common_translation.wml:16
msgid "debian, GNU, linux, unix, open source, free, DFSG"
msgstr "debian, GNU, linux, unix, open source, libero, DFSG"

#: ../../english/template/debian/common_translation.wml:19
msgid "Back to the <a href=\"m4_HOME/\">Debian Project homepage</a>."
msgstr ""
"Ritorna alla pagina principale del <a href=\"m4_HOME/\">Progetto Debian</a>."

#: ../../english/template/debian/common_translation.wml:22
#: ../../english/template/debian/links.tags.wml:149
msgid "Home"
msgstr "Pagina principale"

#: ../../english/template/debian/common_translation.wml:25
msgid "Skip Quicknav"
msgstr "Salta la barra di navigazione"

#: ../../english/template/debian/common_translation.wml:28
msgid "About"
msgstr "Informazioni"

#: ../../english/template/debian/common_translation.wml:31
msgid "About Debian"
msgstr "Informazioni su Debian"

#: ../../english/template/debian/common_translation.wml:34
msgid "Contact Us"
msgstr "Contatti"

#: ../../english/template/debian/common_translation.wml:37
msgid "Legal Info"
msgstr "Informazioni legali"

#: ../../english/template/debian/common_translation.wml:40
msgid "Data Privacy"
msgstr "Privacy dei dati"

#: ../../english/template/debian/common_translation.wml:43
msgid "Donations"
msgstr "Donazioni"

#: ../../english/template/debian/common_translation.wml:46
msgid "Events"
msgstr "Eventi"

#: ../../english/template/debian/common_translation.wml:49
msgid "News"
msgstr "Notizie"

#: ../../english/template/debian/common_translation.wml:52
msgid "Distribution"
msgstr "Distribuzione"

#: ../../english/template/debian/common_translation.wml:55
msgid "Support"
msgstr "Supporto"

#: ../../english/template/debian/common_translation.wml:58
msgid "Pure Blends"
msgstr "Pure Blends"

#: ../../english/template/debian/common_translation.wml:61
#: ../../english/template/debian/links.tags.wml:46
msgid "Developers' Corner"
msgstr "Angolo degli sviluppatori"

#: ../../english/template/debian/common_translation.wml:64
msgid "Documentation"
msgstr "Documentazione"

#: ../../english/template/debian/common_translation.wml:67
msgid "Security Information"
msgstr "Informazioni sulla sicurezza"

#: ../../english/template/debian/common_translation.wml:70
msgid "Search"
msgstr "Cerca"

#: ../../english/template/debian/common_translation.wml:73
msgid "none"
msgstr "nessuno"

#: ../../english/template/debian/common_translation.wml:76
msgid "Go"
msgstr "Vai"

#: ../../english/template/debian/common_translation.wml:79
msgid "worldwide"
msgstr "globale"

#: ../../english/template/debian/common_translation.wml:82
msgid "Site map"
msgstr "Mappa del sito"

#: ../../english/template/debian/common_translation.wml:85
msgid "Miscellaneous"
msgstr "Varie"

#: ../../english/template/debian/common_translation.wml:88
#: ../../english/template/debian/links.tags.wml:104
msgid "Getting Debian"
msgstr "Ottenere Debian"

#: ../../english/template/debian/common_translation.wml:91
msgid "The Debian Blog"
msgstr "Il Blog Debian"

#: ../../english/template/debian/ddp.wml:6
msgid ""
"Please send all comments, criticisms and suggestions about these web pages "
"to our <a href=\"mailto:debian-doc@lists.debian.org\">mailing list</a>."
msgstr ""
"Inviare qualsiasi commento, critica o suggerimento riguardo a queste pagine "
"web alla lista <a href=\"mailto:debian-doc@lists.debian.org\">debian-doc</a>."

#: ../../english/template/debian/fixes_link.wml:11
msgid "not needed"
msgstr "non richiesto"

#: ../../english/template/debian/fixes_link.wml:14
msgid "not available"
msgstr "non disponibile"

#: ../../english/template/debian/fixes_link.wml:17
msgid "N/A"
msgstr "N/D"

#: ../../english/template/debian/fixes_link.wml:20
msgid "in release 1.1"
msgstr "nella versione 1.1"

#: ../../english/template/debian/fixes_link.wml:23
msgid "in release 1.3"
msgstr "nella versione 1.3"

#: ../../english/template/debian/fixes_link.wml:26
msgid "in release 2.0"
msgstr "nella versione 2.0"

#: ../../english/template/debian/fixes_link.wml:29
msgid "in release 2.1"
msgstr "nella versione 2.1"

#: ../../english/template/debian/fixes_link.wml:32
msgid "in release 2.2"
msgstr "nella versione 2.2"

#. TRANSLATORS: Please make clear in the translation of the following
#. item that mail sent to the debian-www list *must* be in English. Also,
#. you can add some information of your own translation mailing list
#. (i.e. debian-l10n-XXXXXX@lists.debian.org) for reporting things in
#. your language.
#: ../../english/template/debian/footer.wml:107
msgid ""
"To report a problem with the web site, please e-mail our publicly archived "
"mailing list <a href=\"mailto:debian-www@lists.debian.org\">debian-www@lists."
"debian.org</a> in English.  For other contact information, see the Debian <a "
"href=\"m4_HOME/contact\">contact page</a>. Web site source code is <a href="
"\"https://salsa.debian.org/webmaster-team/webwml\">available</a>."
msgstr ""
"Per segnalare un problema con il sito web si prega di inviare una e-mail "
"<strong>in inglese</strong> a <a href=\"mailto:debian-www@lists.debian.org"
"\">debian-www@lists.debian.org</a>. Segnalazioni in italiano possono essere "
"inviate alla <a href=\"mailto:debian-l10n-italian@lists.debian.org\">lista "
"di messaggi dei traduttori Debian italiani</a>. Vedere la <a href=\"m4_HOME/"
"contact\">pagina dei contatti</a> Debian per altre informazioni su come "
"contattarci. Il codice sorgente del sito web è <a href=\"m4_HOME/devel/"
"website/using_cvs\">a disposizione</a>."

#: ../../english/template/debian/footer.wml:110
msgid "Last Modified"
msgstr "Ultima modifica"

#: ../../english/template/debian/footer.wml:113
msgid "Last Built"
msgstr "Ultima compilazione"

#: ../../english/template/debian/footer.wml:116
msgid "Copyright"
msgstr "Copyright"

#: ../../english/template/debian/footer.wml:119
msgid "<a href=\"https://www.spi-inc.org/\">SPI</a> and others;"
msgstr "<a href=\"https://www.spi-inc.org/\">SPI</a> e altri;"

#: ../../english/template/debian/footer.wml:122
msgid "See <a href=\"m4_HOME/license\" rel=\"copyright\">license terms</a>"
msgstr ""
"Vedere <a href=\"m4_HOME/license\" rel=\"copyright\">i termini della "
"licenza</a>"

#: ../../english/template/debian/footer.wml:125
msgid ""
"Debian is a registered <a href=\"m4_HOME/trademark\">trademark</a> of "
"Software in the Public Interest, Inc."
msgstr ""
"Debian è un <a href=\"m4_HOME/trademark\">marchio registrato</a> da Software "
"in the Public Interest, Inc."

#: ../../english/template/debian/languages.wml:196
#: ../../english/template/debian/languages.wml:232
msgid "This page is also available in the following languages:"
msgstr "Questa pagina è disponibile anche nelle lingue seguenti:"

#: ../../english/template/debian/languages.wml:265
msgid "How to set <a href=m4_HOME/intro/cn>the default document language</a>"
msgstr ""
"Come configurare <a href=m4_HOME/intro/cn>la lingua predefinita per i "
"documenti</a>"

#: ../../english/template/debian/links.tags.wml:4
msgid "Debian International"
msgstr "Debian diventa Internazionale"

#: ../../english/template/debian/links.tags.wml:7
msgid "Partners"
msgstr "Partner"

#: ../../english/template/debian/links.tags.wml:10
msgid "Debian Weekly News"
msgstr "Notizie settimanali Debian"

#: ../../english/template/debian/links.tags.wml:13
msgid "Weekly News"
msgstr "Notizie settimanali"

#: ../../english/template/debian/links.tags.wml:16
msgid "Debian Project News"
msgstr "Debian Project News"

#: ../../english/template/debian/links.tags.wml:19
msgid "Project News"
msgstr "Notizie dal progetto"

#: ../../english/template/debian/links.tags.wml:22
msgid "Release Info"
msgstr "Informazioni sulle Versioni"

#: ../../english/template/debian/links.tags.wml:25
msgid "Debian Packages"
msgstr "Pacchetti Debian"

#: ../../english/template/debian/links.tags.wml:28
msgid "Download"
msgstr "Scaricare"

#: ../../english/template/debian/links.tags.wml:31
msgid "Debian&nbsp;on&nbsp;CD"
msgstr "Debian&nbsp;su&nbsp;CD"

#: ../../english/template/debian/links.tags.wml:34
msgid "Debian Books"
msgstr "Libri su Debian"

#: ../../english/template/debian/links.tags.wml:37
msgid "Debian Wiki"
msgstr "Debian Wiki"

#: ../../english/template/debian/links.tags.wml:40
msgid "Mailing List Archives"
msgstr "Archivi delle liste di messaggi"

#: ../../english/template/debian/links.tags.wml:43
msgid "Mailing Lists"
msgstr "Liste di messaggi"

#: ../../english/template/debian/links.tags.wml:49
msgid "Social Contract"
msgstr "Il nostro contratto sociale"

#: ../../english/template/debian/links.tags.wml:52
msgid "Code of Conduct"
msgstr "Codice di condotta"

#: ../../english/template/debian/links.tags.wml:55
msgid "Debian 5.0 - The universal operating system"
msgstr "Debian 5.0 - il Sistema Operativo Universale"

#: ../../english/template/debian/links.tags.wml:58
msgid "Site map for Debian web pages"
msgstr "Mappa del sito per le pagine Debian"

#: ../../english/template/debian/links.tags.wml:61
msgid "Developer Database"
msgstr "Base di dati degli sviluppatori"

#: ../../english/template/debian/links.tags.wml:64
msgid "Debian FAQ"
msgstr "FAQ Debian"

#: ../../english/template/debian/links.tags.wml:67
msgid "Debian Policy Manual"
msgstr "Manuale delle norme Debian"

#: ../../english/template/debian/links.tags.wml:70
msgid "Developers' Reference"
msgstr "Guida dello sviluppatore Debian"

#: ../../english/template/debian/links.tags.wml:73
msgid "New Maintainers' Guide"
msgstr "Guida del nuovo manutentore"

#: ../../english/template/debian/links.tags.wml:76
msgid "Release Critical Bugs"
msgstr "Problemi critici"

#: ../../english/template/debian/links.tags.wml:79
msgid "Lintian Reports"
msgstr "Rapporti di lintian"

#: ../../english/template/debian/links.tags.wml:83
msgid "Archives for users' mailing lists"
msgstr "Archivi delle liste di messaggi per utenti"

#: ../../english/template/debian/links.tags.wml:86
msgid "Archives for developers' mailing lists"
msgstr "Archivi delle liste di messaggi per sviluppatori"

#: ../../english/template/debian/links.tags.wml:89
msgid "Archives for i18n/l10n mailing lists"
msgstr "Archivi delle liste di messaggi i18n/l10n"

#: ../../english/template/debian/links.tags.wml:92
msgid "Archives for ports' mailing lists"
msgstr "Archivi delle liste di messaggi per i port"

#: ../../english/template/debian/links.tags.wml:95
msgid "Archives for mailing lists of the Bug tracking system"
msgstr "Archivi delle liste di messaggi del sistema di tracciamento dei bug"

#: ../../english/template/debian/links.tags.wml:98
msgid "Archives for miscellaneous mailing lists"
msgstr "Archivi delle liste di messaggi varie"

#: ../../english/template/debian/links.tags.wml:101
msgid "Free Software"
msgstr "Software Libero"

#: ../../english/template/debian/links.tags.wml:107
msgid "Development"
msgstr "Sviluppo"

#: ../../english/template/debian/links.tags.wml:110
msgid "Help Debian"
msgstr "Aiutare Debian"

#: ../../english/template/debian/links.tags.wml:113
msgid "Bug reports"
msgstr "Segnalazioni di bug"

#: ../../english/template/debian/links.tags.wml:116
msgid "Ports/Architectures"
msgstr "Port/Architetture"

#: ../../english/template/debian/links.tags.wml:119
msgid "Installation manual"
msgstr "Manuale d'installazione"

#: ../../english/template/debian/links.tags.wml:122
msgid "CD vendors"
msgstr "Rivenditori di CD"

#: ../../english/template/debian/links.tags.wml:125
msgid "CD/USB ISO images"
msgstr "Immagini ISO per CD/USB"

#: ../../english/template/debian/links.tags.wml:128
msgid "Network install"
msgstr "Installazione via rete"

#: ../../english/template/debian/links.tags.wml:131
msgid "Pre-installed"
msgstr "Preinstallato"

#: ../../english/template/debian/links.tags.wml:134
msgid "Debian-Edu project"
msgstr "Progetto Debian-Edu"

#: ../../english/template/debian/links.tags.wml:137
msgid "Alioth &ndash; Debian GForge"
msgstr "Alioth &ndash; Debian GForge"

#: ../../english/template/debian/links.tags.wml:140
msgid "Quality Assurance"
msgstr "Controllo qualità"

#: ../../english/template/debian/links.tags.wml:143
msgid "Package Tracking System"
msgstr "Sistema di tracciamento dei pacchetti"

#: ../../english/template/debian/links.tags.wml:146
msgid "Debian Developer's Packages Overview"
msgstr "Introduzione ai pacchetti per sviluppatori Debian"

#: ../../english/template/debian/navbar.wml:10
msgid "Debian Home"
msgstr "Pagina principale di Debian"

#: ../../english/template/debian/recent_list.wml:7
msgid "No items for this year."
msgstr "Nessun articolo per quest'anno."

#: ../../english/template/debian/recent_list.wml:11
msgid "proposed"
msgstr "proposto"

#: ../../english/template/debian/recent_list.wml:15
msgid "in discussion"
msgstr "in discussione"

#: ../../english/template/debian/recent_list.wml:19
msgid "voting open"
msgstr "votazioni aperte"

#: ../../english/template/debian/recent_list.wml:23
msgid "finished"
msgstr "terminato"

#: ../../english/template/debian/recent_list.wml:26
msgid "withdrawn"
msgstr "ritirato"

#: ../../english/template/debian/recent_list.wml:30
msgid "Future events"
msgstr "Eventi futuri"

#: ../../english/template/debian/recent_list.wml:33
msgid "Past events"
msgstr "Eventi passati"

#: ../../english/template/debian/recent_list.wml:37
msgid "(new revision)"
msgstr "(nuova revisione)"

#: ../../english/template/debian/recent_list.wml:303
msgid "Report"
msgstr "Segnalazione"

#. given a manual name and an architecture, join them
#. if you need to reorder the two, use "%2$s ... %1$s", cf. printf(3)
#: ../../english/template/debian/release.wml:7
msgid "<void id=\"doc_for_arch\" />%s for %s"
msgstr "<void id=\"doc_for_arch\" />%s per %s"

#: ../../english/template/debian/translation-check.wml:37
msgid ""
"<em>Note:</em> The <a href=\"$link\">original document</a> is newer than "
"this translation."
msgstr ""
"<em>Nota:</em> <a href=\"$link\">L'originale</a> è più recente di questa "
"traduzione."

#: ../../english/template/debian/translation-check.wml:43
msgid ""
"Warning! This translation is too out of date, please see the <a href=\"$link"
"\">original</a>."
msgstr ""
"Attenzione! Questa traduzione è ormai datata, perciò dai un'occhiata anche "
"alla nuova pagina <a href=\"$link\">originale</a>."

#: ../../english/template/debian/translation-check.wml:49
msgid ""
"<em>Note:</em> The original document of this translation no longer exists."
msgstr "<em>Nota:</em> La pagina originale non esiste più."

#: ../../english/template/debian/translation-check.wml:56
msgid "Wrong translation version!"
msgstr "Versione della traduzione non corretta!"

#: ../../english/template/debian/url.wml:4
msgid "URL"
msgstr "URL"

#: ../../english/template/debian/users.wml:7
msgid "Back to the <a href=\"../\">Who's using Debian? page</a>."
msgstr "Ritorna alla <a href=\"../\">pagina Chi usa Debian?</a>."

#: ../../english/search.xml.in:7
msgid "Debian website"
msgstr "Sito web Debian"

#: ../../english/search.xml.in:9
msgid "Search the Debian website."
msgstr "Cerca nel sito web Debian."

#~ msgid "Visit the site sponsor"
#~ msgstr "Visita il sito dello sponsor"
